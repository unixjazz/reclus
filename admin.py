'''
 Reclus: admin
 Admin interface custom views
'''

from app import app, db, redirect, url_for, render_template
from model import *
from flask_login import current_user
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_admin.form import SecureForm
from wtforms import PasswordField
from werkzeug.security import generate_password_hash, check_password_hash

# Protected main admin view
class AdminProtectedView(AdminIndexView):
    # main view
    def is_accessible(self):
        if not current_user.is_authenticated:
            return False
        return current_user.has_roles('admin')

    def _handle_view(self, name):
        if not self.is_accessible():
            return redirect(url_for('login'))


# Customized admin views
class UserView(ModelView):
    form_base_class = SecureForm
    form_columns = ['username', 'email', 'roles', 'active']
    column_searchable_list = ('username', 'email')
    column_filters = ('username', 'email')
    column_exclude_list = ('pwdhash',)
    column_auto_select_related = True
    column_hide_backrefs = False

    def is_accessible(self):
        if not current_user.is_authenticated:
            return False
        return current_user.has_roles('admin')

    def _handle_view(self, name):
        if not self.is_accessible():
            return redirect(url_for('login'))


class RoleView(ModelView):
    form_base_class = SecureForm

    def is_accessible(self):
        if not current_user.is_authenticated:
            return False
        return current_user.has_roles('admin')

    def _handle_view(self, name):
        if not self.is_accessible():
            return redirect(url_for('login'))


# User form views
class ProtectionFormView(ModelView):
    form_base_class = SecureForm
    column_searchable_list = ('municipality', 'email', 'state', 'lat', 'lng')

    def is_accessible(self):
        if not current_user.is_authenticated:
            return False
        return current_user.has_roles('admin')

    def _handle_view(self, name):
        if not self.is_accessible():
            return redirect(url_for('login'))


class HausJusticeView(ModelView):
    form_base_class = SecureForm
    column_searchable_list = ('Location', 'Latitude', 'Longitude', 'Email', 'Social_Media')

    def is_accessible(self):
        if not current_user.is_authenticated:
            return False
        return current_user.has_roles('admin')

    def _handle_view(self, name):
        if not self.is_accessible():
            return redirect(url_for('login'))


# Admin extension
admin = Admin(app, name='Reclus', index_view=AdminProtectedView(url='/admin'))

# Admin views: registration
admin.add_view(UserView(User, db.session, name='Users'))
admin.add_view(RoleView(Role, db.session, name='Roles'))
admin.add_view(ProtectionFormView(LegisProtection, db.session))
admin.add_view(HausJusticeView(HausJustice, db.session))
