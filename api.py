'''
 Reclus: REST API
 Implements REST Web API support
'''

import io
import csv

from flask import jsonify, make_response
from app import app, db, ma
from model import *

# DB schemas for serialization
# Protection legislation + Housing Justice
class ProtLegisSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ('municipality',
                  'state',
                  'country',
                  'ISO',
                  'admin_scale',
                  'lat',
                  'lon',
                  'passed',
                  'range',
                  'policy_type',
                  'policy_summary',
                  'start',
                  'end',
                  'link'
                  'has_expired_protetions')
        ordered = True


class HausJusticeSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ('Strike_Status',
                  'Start',
                  'Location',
                  'Latitude',
                  'Longitude',
                  'Why',
                  'Resources')
        ordered = True


# Convert serialized JSON to CSV
def json2csv(json_data):
    csv_result = io.StringIO()
    csv_writer = csv.writer(csv_result)
    c = 0
    for item in json_data:
        if c == 0:
            csv_header = item.keys()
            csv_writer.writerow(csv_header)
            c += 1
        csv_writer.writerow(item.values())
    output = csv_result.getvalue()
    csv_result.close()
    return output

@app.route('/api/protlegis/<format>', methods=['GET'])
def getprotform(format):
    if format=='json':
        protlegis_schema = ProtLegisSchema(many=True)
        data = LegisProtection.query.all()
        result = protlegis_schema.dump(data)
        return jsonify(result)
    elif format=='csv':
        protlegis_schema = ProtLegisSchema(many=True)
        data = LegisProtection.query.all()
        result = protlegis_schema.dump(data)
        return json2csv(result), {'Content-Type': 'text/csv'}
    else:
        return jsonify({'error': 'invalid format, try json or csv'})

@app.route('/api/hausjustice/<format>', methods=['GET'])
def gethausjustice(format):
    if format=='json':
        hausjustice_schema = HausJusticeSchema(many=True)
        data = HausJustice.query.all()
        result = hausjustice_schema.dump(data)
        return jsonify(result)
    elif format=='csv':
        hausjustice_schema = HausJusticeSchema(many=True)
        data = HausJustice.query.all()
        result = hausjustice_schema.dump(data)
        return json2csv(result), {'Content-Type': 'text/csv'}
    else:
        return jsonify({'error': 'invalid format, try json or csv'})
