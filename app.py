'''
 Reclus: app
 Main instance for E. Reclus
'''

from flask import Flask, render_template, request, flash, \
    redirect, url_for, session
from flask_security import SQLAlchemyUserDatastore
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.config.from_pyfile('conf/reclus.conf')

# Login manager
login_manager = LoginManager(app)
login_manager.session_protection = "strong"
login_manager.login_view = 'login'

# DB init
db = SQLAlchemy(app)

# Flask migrate
migrate = Migrate(app, db)

# Blueprints for REST API
ma = Marshmallow(app)

# Bootstrap
bootstrap = Bootstrap(app)

# WSGI support for reverse proxy setup
#app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_host=1, num_proxies=1)
