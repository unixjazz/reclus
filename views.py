'''
 Reclus: views
 Describes the public / private views
'''

import gc

from flask import abort
from urllib.parse import urlparse, urljoin
from flask_login import current_user, login_user, logout_user, login_required
from flask_babelex import Babel, gettext, lazy_gettext
from app import app, request, render_template, flash, redirect, url_for, session
from model import *
from forms import *

# i18n support
#babel = Babel(app)
#
#@babel.localeselector
#def get_locale():
#    return request.accept_languages.best_match(app.config['LANGUAGES'])
#    #g.get('lang_code', app.config['BABEL_DEFAULT_LOCALE'])
#
#@babel.timezoneselector
#def get_timezone():
#    app.timezone = 'BABEL_DEFAULT_TIMEZONE'
#

# HTTP error handling
@app.errorhandler(401)
def custom_401(error):
    return redirect(url_for('login'))

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500

# Application endpoints, aka 'routes'
# Login route
@app.route('/login', methods=['GET', 'POST'])
def login():
    next = request.args.get('next')
    if current_user.is_authenticated:
        return redirect(next or url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Credentials are not quite right, try a different combination')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        if not is_safe_url(next):
            return abort(400)
        return redirect(next or url_for('admin'))
    return render_template('login.html', form=form)

# Source: Flask-login doc, safe-url redirect handler:
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

# Register route
@app.route('/register', methods=['GET', 'POST'])
def register():
    first_user = User.query.get(1)
    if first_user == None or (current_user.is_authenticated and current_user.has_roles('admin')):
        form = RegistrationForm()
        if request.method == 'POST':
            if form.validate():
                username = request.form.get('username')
                email = request.form.get('email')
                password = request.form.get('password')
                existing_username = User.query.filter_by(username=username).first()
                if first_user == None:
                    first_role = Role(name='admin')
                    user = User(username, email, password, active=True, roles=[first_role])
                else:
                    user = User(username, email, password, active=False, roles=[])
                db.session.add(user)
                db.session.commit()
                return redirect(url_for('login'))
            else:
                flash(form.errors, 'danger')
                return redirect(url_for('register'))
        else:
            return render_template('register.html', form=form)
    else:
        flash('Ask a friend with access to register an account.')
        return redirect(url_for('login'))

# User log-out
@app.route('/logout')
@login_required
def logout():
    logout_user()
    gc.collect()
    return redirect(url_for('index'))

@login_manager.user_loader
def load_user(user_id):
    try:
        return User.query.get(user_id)
    except:
        return None

# Webpage routes
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/admin')
@login_required
def admin():
    if current_user.is_authenticated and current_user.has_roles('admin'):
        return redirect(url_for('admin'))
    else:
        return redirect(url_for('login'))

@app.route('/map')
def map():
    return redirect('https://mapacovid19.online')

@app.route('/form/protlegis', methods=['GET', 'POST'])
def protlegis():
    form = LegisProtectionForm()
    if request.method == 'POST':
        if form.validate():
            data = LegisProtection(form.email.data,
                                   form.municipality.data,
                                   form.state.data,
                                   form.country.data,
                                   form.admin_scale.data,
                                   form.lat.data,
                                   form.lon.data,
                                   form.passed.data,
                                   form.policy_type.data,
                                   form.policy_summary.data,
                                   form.start.data,
                                   form.end.data,
                                   form.link.data,
                                   form.resources.data,
                                   form.feedback.data)
            db.session.add(data)
            db.session.commit()
            return redirect(url_for('index'))
        else:
            return render_template('protlegis.html', form=form)
    else:
        return render_template('protlegis.html', form=form)

@app.route('/form/hausjustice', methods=['GET', 'POST'])
def hausjustice():
    form = HausJusticeForm()
    if request.method == 'POST':
        if form.validate():
            data = HausJustice(form.Strike_Type.data,
                               form.Strike_Status.data,
                               form.Start.data,
                               form.Location.data,
                               form.Latitude.data,
                               form.Longitude.data,
                               form.Why.data,
                               form.Landlord.data,
                               form.Mobilization_Number.data,
                               form.Units_Housing.data,
                               form.Group_Contact.data,
                               form.Desire_Contact.data,
                               form.Capacity_Contact.data,
                               form.Resources.data,
                               form.Interview.data,
                               form.Email.data,
                               form.Social_Media.data,
                               form.Harassment.data)
            db.session.add(data)
            db.session.commit()
            return redirect(url_for('index'))
        else:
            return render_template('hausjustice.html', form=form)
    else:
        return render_template('hausjustice.html', form=form)
