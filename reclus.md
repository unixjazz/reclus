# Reclus: your friendly counter-mapper!

Reclus is an engine for creating simple web applications for data
gathering and mapping. It is designed around KISS + anticapitalist 
principles and meant to be concise and extensible, serving as a 
backend for your counter-mapping needs.

## Getting started

Make sure you have these pre-requisites installed before you get started:

- Python >= 3.7
- Python3-venv
- Python3-pip
- Python3-wheel
- Python3-dev
- Postgres >= 10
- Libpq-dev

After these are satisfied, create a virtual environment and proceed to install 
the required dependencies for 'reclus':

```
python3 -m venv $YOUR_VENV
source $YOUR_VENV/bin/activate
pip install -r requirements.txt
```

Next, configure the application by editing the files 'conf/reclus.conf' and '.flaskenv'.
After setting up your db credentials and environment variables, create your db using your 
favorite client. On 'psql', you could use these commands:

```
psql# create database reclus;
psql# create user reclus with encrypted password 'YOUR_PASSWD';
psql# grant all privileges on database reclus to reclus;
```

The database structure will be created when you first run 'reclus.py'. Before you proceed, however,
make sure to initialize your db migration scheme. This is useful for potential future upgrades
of the db schema:

```
flask db init && flask db migrate
```

You are now ready to start the application:

```
python reclus.py
```

## API

Only 'GET' requests are implemented at the moment through the following endpoints:
(see api.py for details)

```
127.0.0.1:5000/api/form/$NAME_OF_YOUR_FORM/json (for JSON)
127.0.0.1:5000/api/form/$NAME_OF_YOUR_FORM/csv (for CSV)
```

## TODO

* Reclus + uWSGI + Tor documentation
* Style and templates for the application
* CRUD operations and auth for the REST API
* Ansible provisioning scripts
* Map integration (mapbox)
* Form generator UI

## Licensing

See LICENSE and AUTHORS for details.

This work was inspired by E. Reclus and the AEMP collective, fighting the good fight then and now!
Visit [Anti-Eviction Mapping Project](https://www.antievictionmap.org) to learn about the work.
