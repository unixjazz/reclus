'''
 Reclus 0.1:
 Your counter mapping friend!
'''

from app import app, db
from admin import *
from model import *
from views import *
from forms import *
from api import *

if __name__ == '__main__':
    db.init_app(app)
    db.create_all()
    login_manager.init_app(app)
    app.run()
