'''
 Reclus: forms
 Includes the description of system & user forms
'''

from flask_wtf import FlaskForm
from wtforms import validators, StringField, BooleanField, TextField, PasswordField, \
                    RadioField, DateField, SubmitField
from wtforms.validators import DataRequired, Email, InputRequired, EqualTo, NumberRange
from wtforms.widgets import TextArea

# Login / registering forms
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired()])
    password = PasswordField('Password', validators=[InputRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired()])
    email = StringField('Email', validators=[InputRequired(), Email()])
    password = PasswordField('Password', validators=[
                                            InputRequired(),
                                            EqualTo('confirm',
                                            message='Passwords must match unfortunately =)')])
    confirm = PasswordField('Confirm Password', validators=[InputRequired()])
    submit = SubmitField('Register')


# AEMP-specific forms
# Housing Justice Action Form

class HausJusticeForm(FlaskForm):
    page_title = 'Ação pelo Direito à Moradia'
    form_title = 'Ação pelo Direito à Moradia'
    form_intro = '''Em função da crise do coronavírus, estamos mapeando e coletando informações        \
                    para ajudar a organizar campanhas em defesa do direito à moradia em escala         \
                    nacional. Por favor, ajude-nos neste projeto preenchendo o formulário abaixo.      \
                    As informações compartilhadas ajudarão às organizações de apoio na luta pelo       \
                    acesso à moradia para encontrar as pessoas e os comunidades com maior necessidade  \
                    e fortificar laços de solidariedade e ajuda mútua.'''
    Strike_Type = RadioField('1. Você está participando de alguma ação em defesa do direito  \
                                 à moradia? Que tipo de ação?',
                                validators=[InputRequired('[* Este campo é requerido ]')],
                                choices=[
                                    ('rent_suspension', 'Greve, redução ou suspensão do aluguel'),
                                    ('occupation', 'Ocupação / assentamento'),
                                    ('mutual_aid', 'Apoio mútuo / Apoio direto (ex.: apoio jurídico)'),
                                    ('campaign', 'Campanha (exemplo: apoio à familias sem teto)'),
                                    ('other', 'Outra')],
                                    default='rent_suspension')
    Strike_Status = RadioField('2. Esta é a primeira ação em que você (ou seu grupo) está envolvido?',
                           validators=[InputRequired('[* Este campo é requerido ]')],
                           choices=[('Yes / Sí / 是 / Oui', 'Sim'),
                                    ('Unsure / No estoy segurx / 不确定 / Pas sûr.e.s.', 'Não')],
                                    default='Yes / Sí / 是 / Oui')
    Start = StringField('3. Data de inicio da mobilização:', [validators.DataRequired('* Este campo é requerido'),
                                                              validators.Length(min=0, max=10)])
    Location = StringField('4. Localização em que a mobilização está acontecendo:',
                               [validators.DataRequired('* Este campo é requerido')])
    Latitude = StringField('5. Latitude:', [validators.optional(),
                                            validators.length(max=7, message=('Utilize apenas 3 casas decimais \
                                                                          para latitude, exemplo: \
                                                                          -15.794 (Brasília)'))])
    Longitude = StringField('6. Longitude:', [validators.optional(),
                                              validators.length(max=8, message=('Utilize apenas 3 casas decimais \
                                                                          para longitude, exemplo: \
                                                                          -47.882 (Brasília)'))])
    Why = TextField('7. Por que você decidiu participar de uma ação em defesa do direto à moradia? \
                        Motivação pessoal, fatores externos?', [validators.DataRequired('* Este campo é requerido')],
                        widget=TextArea())
    Landlord = StringField('8. Você sabe o nome do proprietário ou da organização responsável pelo imóvel ou terreno?')
    Mobilization_Number = StringField('9. Quantas pessoas estão envolvidas em sua mobilização?')
    Units_Housing = StringField('10. Quantas unidades ou famílias habitam sua comunidade ou prédio?')
    Group_Contact = RadioField('11. Você está em contato com pessoas que alugam / ocupam um imóvel \
                                    do mesmo proprietário?',
                                    choices=[
                                        ('Yes', 'Sim'),
                                        ('No', 'Não')],
                                        default='Yes')
    Desire_Contact = RadioField('12. Você quer entrar em contato com outras pessoas na mesma situação \
                                     na sua região?',
                                     choices=[
                                         ('Yes', 'Sim'),
                                         ('No', 'Não')],
                                       default='Yes')
    Capacity_Contact = RadioField('13. Você tem como entrar em contato com seus vizinhos para compartilhar \
                                       informações e recursos com eles?',
                                       choices=[
                                           ('Yes', 'Sim'),
                                           ('No', 'Não')],
                                           default='Yes')
    Resources = StringField('14. Digite o endereço de um site com informações sobre organizações ou redes  \
                                 existentes em sua cidade ou região para apoiar a mobilização pelo direito \
                                 à moradia:')
    Interview = RadioField('15. Você está interessado em ser contatado para uma entrevista?',
                                       choices=[
                                           ('Yes', 'Sim'),
                                           ('No', 'Não')],
                                           default='No')
    Email = StringField('16. Digite seu email (caso queira entrar em contato):',
                        validators=[validators.optional(), Email('* Utilize um email válido')])
    Social_Media = StringField('17. Digite seu contato nas redes sociais (caso queira entrar em contato):')
    Harassment = TextField('18. Se você foi vítima de ameaças ou assédio de qualquer tipo por parte da \
                            empresa, governo, ou proprietário do imóvel que você ocupa, deixe o seu    \
                            relato abaixo:', widget=TextArea())
    submit = SubmitField('Enviar')


# Protection Legislation Form

class LegisProtectionForm(FlaskForm):
    page_title = 'Coronavírus: Legislação de Proteção à Inquilinos'
    form_title = 'Coronavírus: Legislação de Proteção à Inquilinos'
    form_intro = '''Estamos mapeando as leis e decretos que cidades, estados, e o  \
                    governo federal estão passando (ou que estão para serem        \
                    aprovadas) para proteger os inquilinos que não tem condições   \
                    de pagar aluguel durante a pandemia do Coronavírus. Precisamos \
                    da sua ajuda para construir este mapa de solidariedade!'''
    email = StringField('1. Digite seu email (caso queira entrar em contato):',
                        validators=[validators.optional(), Email('* Utilize um email válido')])
    municipality = StringField('2. Cidade em que a política foi proposta ou aprovada:',
                        validators=[InputRequired('* Este campo é requerido')])
    state = StringField('3. Estado em que a política foi proposta ou aprovada:',
                        validators=[InputRequired('* Este campo é requerido')])
    country = StringField('4. País em que a política foi proposta ou aprovada:')
    admin_scale = RadioField('5. Escala administrativa da política pública:',
                                validators=[InputRequired('[* Este campo é requerido ]')],
                                choices=[
                                ('City', 'Municipal'),
                                ('County', 'Regional'),
                                ('State', 'Estadual'),
                                ('Country', 'Federal')],
                                default='City')
    lat = StringField('6. Latitude:', [validators.optional(),
                                       validators.length(max=7, message=('Utilize apenas 3 casas decimais \
                                                                          para latitude, exemplo: \
                                                                          -15.794 (Brasília)'))])
    lng = StringField('7. Longitude:', [validators.optional(),
                                       validators.length(max=8, message=('Utilize apenas 3 casas decimais \
                                                                          para longitude, exemplo: \
                                                                          -47.882 (Brasília)'))])
    passed = RadioField('8. Esta legislação ou política pública foi aprovada?',
                           validators=[InputRequired('[* Este campo é requerido ]')],
                           choices=[
                           ('TRUE', 'Sim'),
                           ('FALSE', 'Não')],
                           default='TRUE')
    policy_type = RadioField('9. Instrumento ou orgão responsável pela política pública:',
                                validators=[InputRequired('[* Este campo é requerido ]')],
                                choices=[
                                ('1', 'Conselho municipal'),
                                ('2', 'Conselho regional'),
                                ('3', 'Conselho estadual'),
                                ('4', 'Secretaria de habitação'),
                                ('5', 'Ordem judicial'),
                                ('6', 'Ordem executiva municipal'),
                                ('7', 'Legislação municipal'),
                                ('8', 'Legislação estadual'),
                                ('9', 'Legislação federal'),
                                ('10', 'Outro')],
                                default='1')
    policy_summary = TextField('10. Descrição da política pública:', widget=TextArea(),
                               validators=[InputRequired('* Este campo é requerido')])
    start = StringField('11. Data de inicio:', [validators.DataRequired('* Este campo é requerido'),
                                                validators.Length(min=0, max=10)])
    end = StringField('12. Data de término:', [validators.optional(), validators.Length(min=0, max=10)])
    link = StringField('13. Digite aqui o endereço do site com maiores informações sobre a lei ou \
                        política pública:', validators=[InputRequired('* Este campo é requerido')])
    resources = StringField('14. Liste recursos e/ou organizações de sua localidade de \
                             apoio aos grupos de luta pelo acesso à moradia:')
    feedback = TextField('15. Deixe sugestões para o nosso projeto:', widget=TextArea())
    submit = SubmitField('Enviar')
