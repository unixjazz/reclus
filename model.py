'''
 Reclus: db model
 Describes 'reclus' db model
'''

from app import db, login_manager
from datetime import datetime
from flask_security import RoleMixin, UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

# User model
class UsersRoles(db.Model):
    __tablename__ = 'users_roles'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))


class Role(db.Model, RoleMixin):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    pwdhash = db.Column(db.String(256))
    active = db.Column(db.Boolean())
    created_on = db.Column(db.DateTime(), default=datetime.utcnow)
    updated_on = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)
    roles = db.relationship('Role', secondary='users_roles',
                            backref=db.backref('users', lazy='dynamic'))

    def __init__(self, username, email, password, active, roles):
        self.username = username.lower()
        self.email = email.lower()
        self.set_password(password)
        self.active = active
        self.roles = roles

    def set_password(self, password):
        self.pwdhash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pwdhash, password)

    def is_active(self):
        return self.active

    def has_roles(self, *args):
        return set(args).issubset({role.name for role in self.roles})

    def get_id(self):
        return str(self.id)


# Form data model: Housing Protection Legislation
class LegisProtection(db.Model):
    __tablename__ = 'protection_legislation_form'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120))
    municipality = db.Column(db.String(120))
    state = db.Column(db.String(120))
    country = db.Column(db.String(120))
    ISO = db.Column(db.String(3))
    admin_scale = db.Column(db.String(120))
    lat = db.Column(db.String(20))
    lng = db.Column(db.String(20))
    passed = db.Column(db.String(5))
    range = db.Column(db.Integer())
    policy_type = db.Column(db.String(120))
    policy_summary = db.Column(db.Text())
    start = db.Column(db.String(20))
    end = db.Column(db.String(20))
    link = db.Column(db.String(120))
    resources = db.Column(db.String(120))
    feedback = db.Column(db.Text())
    has_expired_protections = db.Column(db.Boolean())
    created_on = db.Column(db.DateTime(), default=datetime.utcnow)
    updated_on = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __init__(self, email, municipality, state, country, admin_scale, lat, lng, passed, range,
                 policy_type, policy_summary, start, end, link, resources, feedback):
        self.email = email
        self.municipality = municipality
        self.state = state
        self.country = 'Brazil' # hardcoded for now
        self.ISO = 'BRA'
        self.admin_scale = admin_scale
        self.lat = lat
        self.lng = lon
        self.passed = passed
        self.range = 1
        self.policy_type = policy_type
        self.policy_summary = policy_summary
        self.start = start
        self.end = end
        self.link = link
        self.resources = resources
        self.feedback = feedback
        self.has_expired_protections = False # hardcoded for now

# Form data model: Housing Justice model
class HausJustice(db.Model):
    __tablename__ = 'housing_action_form'
    id = db.Column(db.Integer, primary_key=True)
    Strike_Type = db.Column(db.String(120))
    Strike_Status = db.Column(db.String(120))
    Start = db.Column(db.String(120))
    Location = db.Column(db.String(120))
    Latitude = db.Column(db.String(20))
    Longitude = db.Column(db.String(20))
    Why = db.Column(db.Text())
    Landlord = db.Column(db.String(120))
    Mobilization_Number = db.Column(db.String(120))
    Units_Housing = db.Column(db.String(120))
    Group_Contact = db.Column(db.String(120))
    Desire_Contact = db.Column(db.String(120))
    Capacity_Contact = db.Column(db.String(120))
    Resources = db.Column(db.String(120))
    Interview = db.Column(db.String(120))
    Email = db.Column(db.String(120))
    Social_Media = db.Column(db.String(120))
    Harassment = db.Column(db.Text())
    created_on = db.Column(db.DateTime(), default=datetime.utcnow)
    updated_on = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __init__(self, Strike_Type, Strike_Status, Start, Location, Latitude, Longitude, Why,
                 Landlord, Mobilization_Number, Units_Housing, Group_Contact, Desire_Contact,
                 Capacity_Contact, Resources, Interview, Email, Social_Media, Harassment):

        self.Strike_Type = Strike_Type
        self.Strike_Status = Strike_Status
        self.Start = Start
        self.Location = Location
        self.Latitude = Latitude
        self.Longitude = Longitude
        self.Why = Why
        self.Landlord = Landlord
        self.Mobilization_Number = Mobilization_Number
        self.Units_Housing = Units_Housing
        self.Group_Contact = Group_Contact
        self.Desire_Contact = Desire_Contact
        self.Capacity_Contact = Capacity_Contact
        self.Resources = Resources
        self.Interview = Interview
        self.Email = Email
        self.Social_Media = Social_Media
        self.Harassment = Harassment


# Helper function: user loader
@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))
